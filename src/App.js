// Modules
import React, { Component } from "react"
import _ from "lodash"
import $ from "jquery"

// Styles
import "./style.sass"

// Layout Elements
import Header from "./components/Header"
import Main from "./components/Main"
import Footer from "./components/Footer"

// Page Elements
import Search from "./components/Search"
import SearchResult from "./components/SearchResult"

// Constants
const API_ENDPOINT = "https://api.giphy.com/v1/gifs/search"
const API_KEY = "lkVmZqtHQMZAePDtSUM3OkBuBFLjzNBb"

export default class App extends Component {
  state = {
    query: "",
    rating: "g",
    limit: 25,
    offset: 0,
    activeFetch: false,
    results: []
  }

  componentDidMount = () => {
    $(window).on("scroll", this.hitBottom)
  }

  hitBottom = () => {
    let elem = $("#results")
    let windowBottom = $(window).scrollTop() + $(window).height()
    let elemBottom = elem.position().top + elem.innerHeight()

    if (elemBottom <= windowBottom) {
      $(document).off("scroll", this.hitBottom)
      this.scrolledToBottom()
    }
  }

  searchChange = e => {
    const { value } = e.target

    if (value !== "") {
      this.fetchQuery(value)
      this.setState({
        query: value
      })
    } else {
      this.setState({
        results: [],
        query: "",
        limit: 25,
        offset: 0
      })
    }
  }

  fetchQuery = q => {
    const { rating, limit, offset } = this.state
    let url = `${API_ENDPOINT}?q=${q}&api_key=${API_KEY}&limit=${limit}&rating=${rating}`

    if (offset > 0) {
      url += `&offset=${offset}`
      fetch(url)
        .then(response => {
          return response.json()
        })
        .then(response => {
          this.setState({
            results: _.uniq(_.concat(this.state.results, response.data))
          })
          document.addEventListener("scroll", this.hitBottom)
        })
    } else {
      fetch(url)
        .then(response => {
          return response.json()
        })
        .then(response => {
          this.setState({
            results: _.uniq(response.data)
          })
          document.addEventListener("scroll", this.hitBottom)
        })
    }
  }

  scrolledToBottom = () => {
    this.setState({
      offset: this.state.offset + this.state.limit
    })
    this.fetchQuery(this.state.query)
  }

  render() {
    const { query, results, offset } = this.state

    return (
      <div className="app">
        <Header>
          <Search query={query} onChange={this.searchChange} searchOffset={offset} />
        </Header>

        <Main>
          <SearchResult results={results} />
        </Main>

        <Footer />
      </div>
    )
  }
}
