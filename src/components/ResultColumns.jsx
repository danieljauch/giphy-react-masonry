// Modules
import React, { Component, Fragment } from "react"

// Local Components
import Result from "./Result"

export default class ResultColumns extends Component {
  shortestColumn = columnHeights => {
    let lowest = Number.POSITIVE_INFINITY
    let lowestColumn = 0

    for (let i = 0, l = columnHeights.length; i < l; i++) {
      if (columnHeights[i].runningHeight < lowest) {
        lowest = columnHeights[i].runningHeight
        lowestColumn = i
      }
    }

    return lowestColumn
  }

  render() {
    const { results, columns, searchOffset } = this.props

    if (columns > 1) {
      let columnList = []
      let imageWidth = 0
      let ratio = 0
      let imageHeight = 0
      let shortest = 0

      for (let i = 0; i < columns; i++) {
        columnList.push({
          runningHeight: 0,
          results: []
        })
      }

      for (let result = 0; result < results.length; result++) {
        imageWidth = parseInt(results[result].images.original.width)
        ratio = 1 / imageWidth
        imageHeight = ratio * parseInt(results[result].images.original.height)
        shortest = this.shortestColumn(columnList)

        columnList[shortest].runningHeight += imageHeight
        columnList[shortest].results.push(results[result])
      }

      return (
        <Fragment>
          {columnList.map((column, _index) => (
            <section className="search__results-column" key={`resultColumn_${_index}`}>
              {column.results &&
                column.results.map(result => {
                  let resultIndex = results.indexOf(result)
                  return (
                    <Result
                      {...result}
                      referenceId={resultIndex}
                      transitionOffset={resultIndex - (searchOffset / columns)}
                      key={`result${result.id}`}
                    />
                  )
                })}
            </section>
          ))}
        </Fragment>
      )
    } else {
      return (
        <Fragment>
          {results.map((result, _index) => (
            <Result
              {...result}
              referenceId={_index}
              transitionOffset={_index - searchOffset}
              key={`result${result.id}`}
            />
          ))}
        </Fragment>
      )
    }
  }
}
