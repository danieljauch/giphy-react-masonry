// Modules
import React, { Component } from "react"
import $ from "jquery"

export default class Result extends Component {
  state = {
    url: this.props.images.original.url,
    showImage: false,
    transitioned: false,
    imageStyle: {
      width: "auto",
      height: "auto"
    }
  }

  componentDidMount = () => {
    this.setImageDimensions()
    this.transitionIn()
  }

  componentWillUnmount = () => {
    clearTimeout(this.prepareImage)
  }

  prepareImage = setTimeout(() => {
    this.setState({
      showImage: true
    })
  }, 250)

  setImageDimensions = () => {
    const { width, height } = this.props.images.original
    const elemWidth = $(`#imageFallback_${this.props.id}`).width()
    const ratio = elemWidth / parseInt(width, 0)
    const ratioHeight = parseInt(height, 0) * ratio

    this.setState({
      imageStyle: {
        width: elemWidth,
        height: ratioHeight
      }
    })
  }

  transitionIn = () => {
    const { transitionOffset } = this.props

    setTimeout(() => {
      this.setState({
        transitioned: true
      })
    }, 50 * transitionOffset)
  }

  render() {
    const { title, id, style, referenceId } = this.props
    const { imageStyle, transitioned, showImage, url } = this.state

    let classList = ["result", "search__result"]

    if (transitioned) {
      classList.push("after-transition-in")
    }

    return (
      <article className={classList.join(" ")} style={style} id={`result_${referenceId}`}>
        <figure className="result__figure">
          <div className="result__image-wrap" id={`imageFallback_${id}`} style={imageStyle}>
            {showImage && <img className="result__image" src={url} alt={title} />}
          </div>
          <figcaption className="result__caption">
            <p>{title}</p>
          </figcaption>
        </figure>
      </article>
    )
  }
}
