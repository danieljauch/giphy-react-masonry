import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart, faCodeBranch } from "@fortawesome/free-solid-svg-icons";

const Footer = props => (
  <footer className="app__footer">
    <div className="credit">
      Made with <FontAwesomeIcon className="footer-icon heart-icon" icon={faHeart} /> by{" "}
      <a href="http://danieljauch.com/">Daniel Jauch</a>. Feel free to{" "}
      <a href="https://bitbucket.org/danieljauch/default-sass/src">check out the code</a> and{" "}
      <FontAwesomeIcon className="footer-icon fork-icon" icon={faCodeBranch} /> it for yourself.
    </div>
  </footer>
);

export default Footer;
