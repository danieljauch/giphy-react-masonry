// Modules
import React, { Component } from "react"
import $ from "jquery"

// Local Components
import ResultColumns from "./ResultColumns"

export default class SearchResult extends Component {
  state = {
    columns: 1
  }

  componentWillMount = () => {
    this.checkWindowWidth()
    $(document).on("resize", this.checkWindowWidth)
  }

  checkWindowWidth = () => {
    const { columns } = this.state
    const windowWidth = $(window).width()

    if (windowWidth >= 1200) {
      if (columns !== 4) {
        this.setState({
          columns: 4
        })
      }
    } else if (windowWidth >= 960) {
      if (columns !== 3) {
        this.setState({
          columns: 3
        })
      }
    } else if (windowWidth >= 768) {
      if (columns !== 2) {
        this.setState({
          columns: 2
        })
      }
    } else {
      if (columns !== 1) {
        this.setState({
          columns: 1
        })
      }
    }
  }

  render() {
    const { results, searchOffset } = this.props
    const { columns } = this.state
    const validSearch = results && results.length > 0
    let classList = ["search__results-wrap"]

    if (!validSearch) {
      classList.push("no-results")
    }

    return (
      <section className={classList.join(" ")} id="results">
        {validSearch ? (
          <ResultColumns results={results} columns={columns} searchOffset={searchOffset} />
        ) : (
          <div className="search__results-none">Enter a search above</div>
        )}
      </section>
    )
  }
}
