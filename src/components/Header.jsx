import React from "react";

const Header = props => <header className="app__header">{props.children}</header>;

export default Header;
