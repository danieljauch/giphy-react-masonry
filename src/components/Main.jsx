import React from "react";

const Main = props => <main className="app__main">{props.children}</main>;

export default Main;
