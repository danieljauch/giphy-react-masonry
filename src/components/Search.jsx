import React from "react";

const Search = props => {
  const { query, onChange } = props;

  return (
    <div className="input__wrap search__wrap">
      <input className="search__field" type="search" placeholder="Search..." autoComplete="true" value={query} onChange={e => onChange(e)} />
    </div>
  );
};

export default Search;
